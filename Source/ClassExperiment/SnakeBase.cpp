// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "runtime/Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 150.f;
	MovementSpeed = 0.3f;
	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(MovementSpeed);

	AddSnakeElement(4);
	
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum, bool fromFood)
{
	int32 MakeFromFood;
	if (fromFood)
	{
		 MakeFromFood = -200;
	}
	else MakeFromFood = 100;
	for (int i = 0; i < ElementsNum; i++)
	{

		FVector NewLocatoin(SnakeElements.Num() * ElementSize, 0, MakeFromFood);
		FTransform NewTransform(NewLocatoin);


	//----------------------------------------------------
	



	//----------------------------------------------------


		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);

	

		//NewSnakeElem->AttachToActor(this,FAttachmentTransformRules::KeepRelativeTransform);

		NewSnakeElem->SnakeOwner = this;

		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			//NewSnakeElem->MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
		}

	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	 //MovementSpeed = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();  


	//AddActorWorldOffset(MovementVector);
	for (int i=SnakeElements.Num()-1;i>0;i--)
	{
		auto CarrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CarrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement,ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		//--------------------------------------- Create Collision --------------------------
		IInteractable* InteractableInterface = Cast <IInteractable>(Other);
		if (InteractableInterface)  // Check for Valid
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

