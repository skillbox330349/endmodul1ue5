// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExampleActor.generated.h"
//#include "string.h"

UENUM(BluePrintType)
enum class EExampleAcClass : uint8
{
	EAC_RED		UMETA(DisplayName = "RED"),
	EAC_GREEN	UMETA(DisplayName = "GREEN"),
	EAC_BLUE	UMETA(DisplayName = "BLUE")
};


USTRUCT(BluePrintType)
struct FMyData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EExampleAcClass EnumValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 IntValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* ActorPtr;

	FMyData()
	{
		EnumValue	= EExampleAcClass::EAC_BLUE;
		IntValue	= 100;
		ActorPtr	= nullptr;
	}

};


UCLASS()
class CLASSEXPERIMENT_API AExampleActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExampleActor();

	UPROPERTY(BlueprintReadOnly)
	EExampleAcClass ExampleEnumValue;
	
	// Test string to UE
	UPROPERTY(BlueprintReadWrite)
	FString TestFrom_VS_FString = TEXT ("Text from VS");
	// Test uint to UE
	UPROPERTY(BlueprintReadWrite)
	uint8   TestFrom_VS_Uint = 25;

	UPROPERTY (BlueprintReadWrite)
	FMyData ExampleStruct;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
