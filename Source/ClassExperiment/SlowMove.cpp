// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowMove.h"
#include "SnakeBase.h"

// Sets default values
ASlowMove::ASlowMove()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASlowMove::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlowMove::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASlowMove::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)  // Slow
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if(Snake->ElementSize>100)
			{
				Snake->ElementSize -= 50;
			}
			else
			{
				Snake->ElementSize = 100.f;
			}
			
		}
	}
}

