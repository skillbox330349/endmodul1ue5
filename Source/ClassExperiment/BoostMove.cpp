 // Fill out your copyright notice in the Description page of Project Settings.


#include "BoostMove.h"
#include "SnakeBase.h"

// Sets default values
ABoostMove::ABoostMove()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABoostMove::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABoostMove::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABoostMove::Interact(AActor* Interactor, bool bIsHead)
{

	if (bIsHead)  // Faster
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if (Snake->ElementSize < 250)
			{
				Snake->ElementSize = Snake->ElementSize + 50;
			}
			
			
		}
	}
}

