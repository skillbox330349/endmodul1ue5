// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
//--------------------------
class AExampleActor;
class ABoostMove;
class ASlowMove;

//--------------------------

bool StartGame=false;

UCLASS()
class CLASSEXPERIMENT_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnMyCamera;

	UPROPERTY(BlueprintReadWrite)
	int VerticalCameraPosition = 0;
	float VerticalCameraRotation = 0.0f;
	
	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;
	UPROPERTY (EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	//-----------------------------------
	UPROPERTY(BlueprintReadWrite)
	AExampleActor* ExampleActor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AExampleActor> ExampleActorClass;

	UPROPERTY(BlueprintReadWrite)
	ABoostMove* BoostActor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABoostMove> BoostActorClass;

	UPROPERTY(BlueprintReadWrite)
	ASlowMove* SlowActor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASlowMove> SlowActorClass;
	//-----------------------------------





protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	void CreateSnakeActor();

	//-----------------------------------
	void CreateExampleActor();
	void CreateBoostActor();
	void CreateSlowActor();
	//-----------------------------------

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);
};
