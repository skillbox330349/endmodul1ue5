// Fill out your copyright notice in the Description page of Project Settings.



#include "PlayerPawnBase.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "SnakeBase.h"
#include "ExampleActor.h"
#include "BoostMove.h"
#include "SlowMove.h"



// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnMyCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnMyCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	CreateSnakeActor();
	// -----------------------------------
	CreateExampleActor();
	CreateBoostActor();
	CreateSlowActor();
	// -----------------------------------
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	// Up and Rotation Camera ---------------------------------------------------------//------------------------------------------------
	SetActorLocation(FVector(0, 0, VerticalCameraPosition));
	if (VerticalCameraPosition <= 6000)
	{
		VerticalCameraPosition = VerticalCameraPosition + (int)(DeltaTime + 50);

		if (VerticalCameraRotation <= 90 && VerticalCameraPosition >= 1000)
		{
			SetActorRotation(FRotator(-VerticalCameraRotation, 0, 0));
			VerticalCameraRotation = VerticalCameraRotation + 0.015 + DeltaTime + 1;
		}
		
	}
	
	// Up and Rotation Camera Cancel ----------------------------------------------------
	

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	//SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(ASnakeBase::StaticClass(),FTransform());
	// new redaction
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(FVector(0, 0, 100)));//--------------------
}

//-----------------------------------
void APlayerPawnBase::CreateExampleActor()
{
	ExampleActor = GetWorld()->SpawnActor<AExampleActor>(ExampleActorClass, FTransform(FVector(-3000, 3000, 100)));
	ExampleActor = GetWorld()->SpawnActor<AExampleActor>(ExampleActorClass, FTransform(FVector( 3000, 3000, 100)));
	ExampleActor = GetWorld()->SpawnActor<AExampleActor>(ExampleActorClass, FTransform(FVector(-3000,-3000, 100)));
	ExampleActor = GetWorld()->SpawnActor<AExampleActor>(ExampleActorClass, FTransform(FVector(3000, -3000, 100)));
}
void APlayerPawnBase::CreateBoostActor()
{
	BoostActor = GetWorld()->SpawnActor<ABoostMove>(BoostActorClass, FTransform(FVector(-1000, 1000, 100)));
}
void APlayerPawnBase::CreateSlowActor()
{
	SlowActor = GetWorld()->SpawnActor<ASlowMove>(SlowActorClass, FTransform(FVector(1000, -1000, 100)));
}
//-----------------------------------

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection!=EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}
