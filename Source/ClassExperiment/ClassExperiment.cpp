// Copyright Epic Games, Inc. All Rights Reserved.

#include "ClassExperiment.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ClassExperiment, "ClassExperiment" );
